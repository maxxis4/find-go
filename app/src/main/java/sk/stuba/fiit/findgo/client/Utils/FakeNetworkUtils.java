package sk.stuba.fiit.findgo.client.Utils;

import android.util.Log;

import java.net.URL;

import sk.stuba.fiit.findgo.client.Data.RouteParams;

/**
 * Fake class for fake server response used testing purposes
 * @author maxxis
 */
public class FakeNetworkUtils implements NetworkUtils {
    private static final String TAG = "FakeNetworkUtils";
    private static final String fakeResponse =
                    "{\n" +
                    "  \"routes\": [\n" +
                    "    {\n" +
                    "      \"start\": \"14:25\",\n" +
                    "      \"end\": \"15:01\",\n" +
                    "      \"buses\":[\n" +
                    "        {\n" +
                    "          \"number\": 39,\n" +
                    "      \n" +
                    "          \"bus_stops\": [\n" +
                    "            {\n" +
                    "              \"name\": \"Zoo\",\n" +
                    "              \"departure\": \"14:30\",\n" +
                    "              \"lat\": 48.153788, \n" +
                    "              \"lon\": 17.076128\n" +
                    "            }, \n" +
                    "            {\n" +
                    "              \"name\": \"Lafrancony\",\n" +
                    "              \"departure\": \"14:35\",\n" +
                    "              \"lat\": 48.145087,\n" +
                    "              \"lon\": 17.078639\n" +
                    "            }\n" +
                    "          ]\n" +
                    "        }\n" +
                    "      ]\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}\n";

    public String getResponseFromHttpUrl(String requestJson, URL serverUrl) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Log.e(TAG, "Error thread sleep", e);
        }
        return fakeResponse;
    }
}
