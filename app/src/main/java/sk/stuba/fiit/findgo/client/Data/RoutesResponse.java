package sk.stuba.fiit.findgo.client.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;
import java.util.List;

/**
 * Class for storing parsing information and all possible routes from server
 * @author maxxis
 */

public class RoutesResponse implements Parcelable{
    public static final String INTENT_NAME = "RoutesResponse";
    public static final String ROUTES = "routes";
    public static final String START_TIME = "start";
    public static final String END_TIME = "end";
    public static final String BUSES = "buses";
    public static final String BUS_NAME = "number";
    public static final String BUS_STOPS = "bus_stops";
    public static final String BUS_STOP_NAME = "name";
    public static final String DEPARTURE = "departure";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";

    private List<RouteInfo> routes = null;

    public RoutesResponse() {
        this.routes = new LinkedList<>();
    }

    protected RoutesResponse(Parcel in) {
        routes = in.createTypedArrayList(RouteInfo.CREATOR);
    }

    public static final Creator<RoutesResponse> CREATOR = new Creator<RoutesResponse>() {
        @Override
        public RoutesResponse createFromParcel(Parcel in) {
            return new RoutesResponse(in);
        }

        @Override
        public RoutesResponse[] newArray(int size) {
            return new RoutesResponse[size];
        }
    };

    public void addRoute(RouteInfo routeInfo){
        routes.add(routeInfo);
    }

    public RouteInfo getRoute(int index){
        if(routes.size() > index){
            return routes.get(index);
        }
        return null;
    }

    public int getSize(){
        return routes.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(routes);
    }
}
