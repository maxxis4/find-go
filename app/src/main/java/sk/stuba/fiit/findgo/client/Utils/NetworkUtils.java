package sk.stuba.fiit.findgo.client.Utils;

import java.net.URL;

import sk.stuba.fiit.findgo.client.Data.RouteParams;

/**
 * Interface for implementation real and testing network response
 * @author maxxis
 */
public interface NetworkUtils {
    String getResponseFromHttpUrl(String requestJson, URL serverUrl);

}
