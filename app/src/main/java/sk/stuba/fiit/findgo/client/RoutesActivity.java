package sk.stuba.fiit.findgo.client;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import sk.stuba.fiit.findgo.client.Data.RouteInfo;
import sk.stuba.fiit.findgo.client.Data.RoutesResponse;

/**
 * Activity to show routes in RecyclerView
 * @author maxxis
 */
public class RoutesActivity extends AppCompatActivity implements RoutesAdapter.ListItemClickListener {

    private static int NUM_LIST_ITEMS = 10;
    private static final String TAG = "RoutesActivity";

    private RoutesAdapter mRoutesAdapter;
    private RecyclerView mRoutesList;

    private RoutesResponse mRoutesResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);

        Intent callingIntent = getIntent();
        mRoutesResponse = callingIntent.getParcelableExtra(RoutesResponse.INTENT_NAME);
        NUM_LIST_ITEMS = mRoutesResponse.getSize();
        mRoutesList = (RecyclerView) findViewById(R.id.rv_routes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRoutesList.setLayoutManager(layoutManager);

        mRoutesAdapter = new RoutesAdapter(NUM_LIST_ITEMS, mRoutesResponse, this);
        mRoutesList.setAdapter(mRoutesAdapter);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        Intent showFullRoute = new Intent(this, FullRouteActivity.class);
        RouteInfo routeInfo = mRoutesResponse.getRoute(clickedItemIndex);
        if(routeInfo != null) {
            showFullRoute.putExtra(RouteInfo.INTENT_NAME, routeInfo);
            startActivity(showFullRoute);
        } else {
            Log.e(TAG,"RecyclerViewItemIndex is out of range");
            throw new NullPointerException();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
