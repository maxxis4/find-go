package sk.stuba.fiit.findgo.client.Utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import sk.stuba.fiit.findgo.client.Data.RouteParams;

/**
 * Class for real network requests and responses
 * @author maxxis
 */
public class RealNetworkUtils implements NetworkUtils {
    private static final String TAG = "RealNetworkUtils";

    public String getResponseFromHttpUrl(String requestJson, URL serverUrl) {
        HttpURLConnection urlConnection;
        String result = null;
        try {
            //Connect
            urlConnection = (HttpURLConnection) ((serverUrl.openConnection()));
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();

            //Write
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(requestJson);
            writer.close();
            outputStream.close();

            int status = urlConnection.getResponseCode();
            Log.i(TAG, "Server status: " + status);
            if(status < 300) {
                //Read
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

                String line = null;
                StringBuilder sb = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }

                bufferedReader.close();
                result = sb.toString();
            } else {
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "Request/response error",e);
        }
        return result;
    }
}
