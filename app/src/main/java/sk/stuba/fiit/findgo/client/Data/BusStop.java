package sk.stuba.fiit.findgo.client.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * This class stores information about bus stops
 * @author maxxis
 */
public class BusStop implements Parcelable {
    private String mName;
    private String mDeparture;
    private LatLng mPosition;

    public BusStop(String name, String departure, double lat, double lon) {
        mName = name;
        mDeparture = departure;
        mPosition = new LatLng(lat, lon);
    }

    protected BusStop(Parcel in) {
        mName = in.readString();
        mDeparture = in.readString();
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<BusStop> CREATOR = new Creator<BusStop>() {
        @Override
        public BusStop createFromParcel(Parcel in) {
            return new BusStop(in);
        }

        @Override
        public BusStop[] newArray(int size) {
            return new BusStop[size];
        }
    };

    public String getName() {
        return mName;
    }

    public String getDeparture() {
        return mDeparture;
    }

    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mDeparture);
        dest.writeParcelable(mPosition, flags);
    }
}
