package sk.stuba.fiit.findgo.client;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import sk.stuba.fiit.findgo.client.Data.RouteParams;
import sk.stuba.fiit.findgo.client.Data.RoutesResponse;
import sk.stuba.fiit.findgo.client.Utils.NetworkUtils;
import sk.stuba.fiit.findgo.client.Utils.RealNetworkUtils;
import sk.stuba.fiit.findgo.client.Utils.RouteJSONUtils;

/**
 * Main Activity of application for collect information about route
 * @author maxxis
 */
public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,
        SharedPreferences.OnSharedPreferenceChangeListener, LoaderManager.LoaderCallbacks<RoutesResponse> {

    private static final String TAG = "MainActivity";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 123;

    private static GoogleApiClient mGoogleApiClient;
    public LocationManager mLocationManager;

    Calendar calendar;
    SimpleDateFormat timeDateFormater;

    private EditText mTimeDateInput;
    private Button mFind;
    private ProgressBar mLoadingIndicator;
    private TextView mErrorMessageDisplay;

    Location mActualPosition;
    Place mFromPlace;
    Place mToPlace;
    Calendar mSelectedTime;
    Boolean timeChanged;

    RouteParams routeParams;

    private static final List<String> BratislavaLocalities = Arrays.asList("Bratislava", "Staré Mesto", "Lamač", "Ružinov",
            "Devín", "Vrakuňa", "Devínska Nová Ves", "Podunajské Biskupice", "Záhorská Bystrica",
            "Nové Mesto", "Petržalka", "Rača", "Jarovce", "Vajnory", "Rusovce", "Karlova Ves",
            "Čunovo", "Dúbravka");
    Geocoder geo = null;
    AlertDialog.Builder mErrorMesseger;

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        geo = new Geocoder(getApplicationContext(), Locale.getDefault());
        setLanguage(sharedPreferences);

        setContentView(R.layout.activity_main);

        mLoadingIndicator = (ProgressBar) findViewById(R.id.pb_loading_indicator);
        mErrorMessageDisplay = (TextView) findViewById(R.id.tv_error_message_display);

        mFind = (Button) findViewById(R.id.find);
        mTimeDateInput = (EditText) findViewById(R.id.time_input);

        calendar = Calendar.getInstance();
        timeDateFormater = new SimpleDateFormat(getString(R.string.time_date_format));
        mSelectedTime =  Calendar.getInstance();

        mTimeDateInput.setHint(R.string.time_today);
        handlePermissionsAndGetLocation();

        timeChanged = false;
        mTimeDateInput.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog time_picker;
                time_picker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker time_picker, int hourOfDay, int minute) {
                        mSelectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        mSelectedTime.set(Calendar.MINUTE, minute);
                        new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                mSelectedTime.set(selectedyear, selectedmonth, selectedday);
                                mTimeDateInput.setText(timeDateFormater.format(mSelectedTime.getTime()));
                                timeChanged = true;
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }, hour, minute, true);
                time_picker.setTitle(getString(R.string.time_picker_title));
                time_picker.show();
            }
        });

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        PlaceAutocompleteFragment autocompleteFragment_to_route = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_to_route);
        autocompleteFragment_to_route.setBoundsBias(new LatLngBounds(
                new LatLng(48.109315, 17.079395),
                new LatLng(48.199625, 17.197535)));

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("SK")
                .build();

        autocompleteFragment_to_route.setFilter(typeFilter);
        autocompleteFragment_to_route.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place: " + place.getName());
                Log.d(TAG, "LatLong: " + place.getLatLng());
                mToPlace = place;
            }

            @Override
            public void onError(Status status) {
                Log.e(TAG, "An error occurred: " + status);
            }
        });

        PlaceAutocompleteFragment autocompleteFragment_from_route = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_from_route);

        autocompleteFragment_from_route.setFilter(typeFilter);
        autocompleteFragment_from_route.setHint(getString(R.string.from_route_hint));

        autocompleteFragment_from_route.setBoundsBias(new LatLngBounds(
                new LatLng(48.109315, 17.079395),
                new LatLng(48.199625, 17.197535)));

        autocompleteFragment_from_route.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG, "Place: " + place.getName());
                Log.d(TAG, "LatLong: " + place.getLatLng());
                mFromPlace = place;
            }

            @Override
            public void onError(Status status) {
                Log.e(TAG, "An error occurred: " + status);
            }
        });

        mFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "request start");
                routeParams = new RouteParams();
                if (mFromPlace == null) {

                    if (mActualPosition != null && checkLocation(new LatLng(mActualPosition.getLatitude(),mActualPosition.getLongitude()))) {
                        routeParams.setFromPlace(mActualPosition);
                    } else {
                        showErrorMessage(getString(R.string.bad_actual_position));
                        Log.w(TAG, "Actual position is outside Bratislava");
                        return;
                    }
                } else {
                    if (checkLocation(mFromPlace.getLatLng())) {
                        routeParams.setFromPlace(mFromPlace.getLatLng());
                    } else {
                        showErrorMessage(getString(R.string.bad_location));
                        Log.w(TAG, "Selected position is outside Bratislava");
                        return;
                    }
                }
                if (mToPlace == null) {
                    Log.w(TAG, "Location is empty");
                    showErrorMessage(getString(R.string.missing_destination));
                    return;
                } else {
                    if (checkLocation(mToPlace.getLatLng())) {
                        routeParams.setToPlace(mToPlace.getLatLng());
                        routeParams.setToPlaceName(mToPlace.getName().toString());
                    } else {
                        showErrorMessage(getString(R.string.bad_location));
                        Log.i(TAG, "Selected destination is outside Bratislava");
                        return;
                    }
                }
                if(!timeChanged){
                    routeParams.setTime(Calendar.getInstance());
                } else {
                    if (mSelectedTime.getTimeInMillis() > calendar.getTimeInMillis()) {
                        routeParams.setTime(mSelectedTime);
                    } else {
                        Log.w(TAG, "Selected time is smaller than actual time");
                        showErrorMessage(getString(R.string.bad_time));
                        return;
                    }
                }

                LoaderManager loaderManager = getSupportLoaderManager();
                Loader<RoutesResponse> routesSearchLoader = loaderManager.getLoader(1);
                if (routesSearchLoader == null) {
                    loaderManager.initLoader(1, null, MainActivity.this);
                } else {
                    loaderManager.restartLoader(1, null, MainActivity.this);
                }
            }
        });
    }

    private boolean checkLocation(LatLng place) {
        try {
            List<Address> addresses = geo.getFromLocation(place.latitude,place.longitude,1);
            if (addresses.isEmpty()) {
                return false;
            } else {
                if (addresses.size() > 0) {
                    Log.i(TAG, "Actual location: " + addresses.get(0).getLocality());
                    return BratislavaLocalities.contains(addresses.get(0).getLocality());
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Unknown location ", e);
            Toast.makeText(getApplicationContext(),R.string.connection_error,Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    @Override
    /**
     * Function for start task in background, used for downloading from server
     */
    public Loader<RoutesResponse> onCreateLoader(int id, final Bundle args) {
        return new AsyncTaskLoader<RoutesResponse>(this) {
            RoutesResponse mRoutesJson;

            @Override
            protected void onStartLoading() {
                mLoadingIndicator.setVisibility(View.VISIBLE);

                if (mRoutesJson != null) {
                    deliverResult(mRoutesJson);
                } else {
                    forceLoad();
                }
            }

            @Override
            public RoutesResponse loadInBackground() {
                URL serverUrl = null;
                try {
                    serverUrl = new URL(getString(R.string.server_url));
                } catch (MalformedURLException e) {
                    Log.e(TAG, "Bad URL parsing",e);
                    return null;
                }
                NetworkUtils networkUtils = new RealNetworkUtils();
                String requestJson = RouteJSONUtils.buildRequestJson(routeParams);
                String routeJSONResult = networkUtils.getResponseFromHttpUrl(requestJson, serverUrl);
                if(routeJSONResult == null){
                    return null;
                }
                return RouteJSONUtils.parseResponse(routeJSONResult, new RoutesResponse(), routeParams);
            }

            @Override
            public void deliverResult(RoutesResponse routesJson) {
                mRoutesJson = routesJson;
                super.deliverResult(mRoutesJson);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<RoutesResponse> loader, RoutesResponse routesResponse) {
        mLoadingIndicator.setVisibility(View.INVISIBLE);

        if (routesResponse == null) {
            showErrorMessage(getString(R.string.server_error));
        } else {
            Log.i(TAG, "Show routes");
            Intent showRoutes = new Intent(MainActivity.this, RoutesActivity.class);
            showRoutes.putExtra(RoutesResponse.INTENT_NAME, routesResponse);
            startActivity(showRoutes);
        }
    }

    @Override
    public void onLoaderReset(Loader<RoutesResponse> loader) {

    }

    private void showErrorMessage(String message) {
        mErrorMesseger = new AlertDialog.Builder(MainActivity.this);
        mErrorMesseger.setMessage(message);
        mErrorMesseger.setCancelable(false);
        mErrorMesseger.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = mErrorMesseger.create();
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onResume() {
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        super.onResume();
    }

    public void setLanguage(SharedPreferences sharedPreferences) {
        String language = sharedPreferences.getString(
                getString(R.string.pref_language_key), getString(R.string.pref_language_def));
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    /**
     * Function for requesting permissions
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Accepted
                    getLocation();
                } else {
                    // Denied
                    Toast.makeText(MainActivity.this, "LOCATION Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handlePermissionsAndGetLocation() {
        Log.v(TAG, "handlePermissionsAndGetLocation");
        int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            return;
        }
        //if already has permission
        getLocation();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void getLocation() {
        Log.v(TAG, "GetLocation");
        final int LOCATION_REFRESH_TIME = 1000;
        final int LOCATION_REFRESH_DISTANCE = 5;

        if (!(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            Log.v(TAG, "Has permission");
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, mLocationListener);
            mActualPosition = mLocationManager.getLastKnownLocation(mLocationManager.getBestProvider(new Criteria(), true));

        } else {
            Log.v(TAG, "Does not have permission");

        }

    }

    /**
     * Listener for location
     */
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "Location Change");
            mActualPosition = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.main_settings:
                Intent start_settings_activity = new Intent(this, SettingsActivity.class);
                startActivity(start_settings_activity);
                Log.d(TAG, "Setting open");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_language_key))) {
            Log.i(TAG, "Changing language");
            setLanguage(sharedPreferences);
            recreate();
        }

    }
}
