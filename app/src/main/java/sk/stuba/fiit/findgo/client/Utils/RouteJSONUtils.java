package sk.stuba.fiit.findgo.client.Utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import sk.stuba.fiit.findgo.client.Data.BusStop;
import sk.stuba.fiit.findgo.client.Data.RouteInfo;
import sk.stuba.fiit.findgo.client.Data.RouteParams;
import sk.stuba.fiit.findgo.client.Data.RoutesResponse;
import sk.stuba.fiit.findgo.client.Data.VehicleRoute;

/**
 * Class for making and parsing JSON for REST architecture
 * @author maxxis
 */
public class RouteJSONUtils {
    private static final String TAG = "RouteJSONUtils";
    private static String TIME_PATTERN = "HH:mm";

    @SuppressLint("SimpleDateFormat")
    /**
     * Building request
     * @param routeParams Main input information about route - from, to, time
     * @return JSON for request
     */
    public static String buildRequestJson(RouteParams routeParams) {
        SimpleDateFormat time_formater;
        time_formater = new SimpleDateFormat(TIME_PATTERN);

        String routeStartingTime = time_formater.format(routeParams.getStartingTime().getTime());

        JSONObject input = new JSONObject();
        try {
            input.put(RouteParams.COLUMN_FROM_LAT, routeParams.getFromLat());
            input.put(RouteParams.COLUMN_FROM_LONG, routeParams.getFromLong());
            input.put(RouteParams.COLUMN_TO_LAT, routeParams.getToLat());
            input.put(RouteParams.COLUMN_TO_LONG, routeParams.getToLong());
            input.put(RouteParams.COLUMN_TIME, routeStartingTime);
        } catch (JSONException e) {
            Log.e(TAG, "JSON error while making input", e);
        }
        Log.i(TAG, input.toString());

        return input.toString();
    }

    /**
     * Parsing response from server
     * @param routeJSONResult RAW JSON from server
     * @param routesResponse Class for storing parsed infomation
     * @param routeParams Class with input information for route
     * @return Class wih all parsed routes
     */
    public static RoutesResponse parseResponse(String routeJSONResult, RoutesResponse routesResponse, RouteParams routeParams) {
        if(routeJSONResult!=null) {
            LatLng start = new LatLng(routeParams.getFromLat(),routeParams.getFromLong());
            LatLng dest = new LatLng(routeParams.getToLat(),routeParams.getToLong());
            try {
                JSONObject jsonObj = new JSONObject(routeJSONResult);
                JSONArray routes = jsonObj.getJSONArray(RoutesResponse.ROUTES);

                for (int i = 0; i < routes.length(); i++) {
                    JSONObject route = routes.getJSONObject(i);

                    String start_time = route.getString(RoutesResponse.START_TIME);
                    String end_time = route.getString(RoutesResponse.END_TIME);
                    List<VehicleRoute> vehicleRouteList = new LinkedList<>();
                    JSONArray buses = route.getJSONArray(RoutesResponse.BUSES);
                    for(int j = 0; j < buses.length();j++){
                        JSONObject bus = buses.getJSONObject(j);

                        String number = bus.getString(RoutesResponse.BUS_NAME);
                        VehicleRoute vehicleRoute = new VehicleRoute(number);

                        JSONArray bus_stops = bus.getJSONArray(RoutesResponse.BUS_STOPS);
                        for(int k = 0; k < bus_stops.length();k++){
                            JSONObject bus_stop = bus_stops.getJSONObject(k);

                            String bus_stop_name = bus_stop.getString(RoutesResponse.BUS_STOP_NAME);
                            String departure = bus_stop.getString(RoutesResponse.DEPARTURE);
                            double lat = bus_stop.getDouble(RoutesResponse.LATITUDE);
                            double lon = bus_stop.getDouble(RoutesResponse.LONGITUDE);
                            BusStop busStop = new BusStop(bus_stop_name,departure,lat,lon);
                            vehicleRoute.addBusStop(busStop);
                        }
                        vehicleRouteList.add(vehicleRoute);
                    }
                    RouteInfo routeInfo = new RouteInfo(start_time,end_time,vehicleRouteList);
                    routeInfo.setStart(start);
                    routeInfo.setDest(dest);
                    routeInfo.setDestName(routeParams.getToPlaceName());
                    routesResponse.addRoute(routeInfo);
                }
            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: ", e);
            }
        }
        return routesResponse;
    }
}
