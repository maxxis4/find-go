package sk.stuba.fiit.findgo.client.Data;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;

/**
 * This class collect input params of route
 * @author maxxis
 */
public class RouteParams implements Parcelable{
    private static final String TAG = "RouteParams";
    public static final String INTENT_NAME = "route_info";

    public static final String COLUMN_FROM_LAT = "from_location_lat";
    public static final String COLUMN_FROM_LONG = "from_location_long";
    public static final String COLUMN_TO_LAT = "to_location_lat";
    public static final String COLUMN_TO_LONG = "to_location_long";
    public static final String COLUMN_TIME = "now_time";

    private double mFromLocationLat;
    private double mFromLocationLong;
    private double mToLocationLat;
    private double mToLocationLong;
    private String mToLocationName;
    private Calendar mTime;


    public RouteParams() {

    }

    private RouteParams(Parcel in) {
        mFromLocationLat = in.readDouble();
        mFromLocationLong = in.readDouble();
        mToLocationLat = in.readDouble();
        mToLocationLong = in.readDouble();
        mToLocationName = in.readString();

        mTime = Calendar.getInstance();
        mTime.setTimeInMillis(in.readLong());
    }

    public static final Creator<RouteParams> CREATOR = new Creator<RouteParams>() {
        @Override
        public RouteParams createFromParcel(Parcel in) {
            return new RouteParams(in);
        }

        @Override
        public RouteParams[] newArray(int size) {
            return new RouteParams[size];
        }
    };

    public void setFromPlace(Location fromPlace) {
        mFromLocationLat = fromPlace.getLatitude();
        mFromLocationLong = fromPlace.getLongitude();
    }

    public void setFromPlace(LatLng fromPlace) {
        mFromLocationLat = fromPlace.latitude;
        mFromLocationLong = fromPlace.longitude;
    }

    public void setToPlace(LatLng toPlace) {
        mToLocationLat = toPlace.latitude;
        mToLocationLong = toPlace.longitude;
    }

    public void setToPlaceName(String name){
        mToLocationName = name;
    }

    public void setTime(Calendar time) {
        mTime = time;
    }

    public String getToPlaceName(){
        return mToLocationName;
    }

    public Calendar getStartingTime() {
        return mTime;
    }

    public double getFromLat() {
        return mFromLocationLat;
    }

    public double getFromLong() {
        return mFromLocationLong;
    }

    public double getToLat() {
        return mToLocationLat;
    }

    public double getToLong() {
        return mToLocationLong;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mFromLocationLat);
        dest.writeDouble(mFromLocationLong);
        dest.writeDouble(mToLocationLat);
        dest.writeDouble(mToLocationLong);
        dest.writeString(mToLocationName);
        dest.writeLong(mTime.getTimeInMillis());
    }

}
