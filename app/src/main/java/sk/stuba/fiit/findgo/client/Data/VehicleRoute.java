package sk.stuba.fiit.findgo.client.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class shows route of bus, with selected bus stops
 * @author maxxis
 */
public class VehicleRoute implements Parcelable{
    private String mName;
    private List<BusStop> mBusStops;
    private int mBusStopsCounter;

    public VehicleRoute(String name){
        mBusStops = new ArrayList<>();
        mBusStopsCounter = 0;
        mName = name;
    }

    protected VehicleRoute(Parcel in) {
        mName = in.readString();
        mBusStopsCounter = in.readInt();
        mBusStops = new ArrayList<>();
        in.readTypedList(mBusStops, BusStop.CREATOR);
    }

    public static final Creator<VehicleRoute> CREATOR = new Creator<VehicleRoute>() {
        @Override
        public VehicleRoute createFromParcel(Parcel in) {
            return new VehicleRoute(in);
        }

        @Override
        public VehicleRoute[] newArray(int size) {
            return new VehicleRoute[size];
        }
    };

    public void addBusStop(BusStop busStop){
        mBusStops.add(busStop);
        mBusStopsCounter++;
    }

    public String getName(){
        return mName;
    }

    public List<BusStop> getBusStops(){
        return mBusStops;
    }

    public BusStop getFirstBusStop(){
        return mBusStops.get(0);
    }

    public BusStop getLastBusStop(){
        return mBusStops.get(mBusStopsCounter-1);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeInt(mBusStopsCounter);
        dest.writeTypedList(mBusStops);
    }
}
