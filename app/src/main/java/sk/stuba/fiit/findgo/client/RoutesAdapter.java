package sk.stuba.fiit.findgo.client;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import sk.stuba.fiit.findgo.client.Data.RouteInfo;
import sk.stuba.fiit.findgo.client.Data.RoutesResponse;
import sk.stuba.fiit.findgo.client.Data.VehicleRoute;

/**
 * Adapter for showing items in RouteActivity with ClickListener
 * @author maxxis
 */
public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.RouteViewHolder> {

    private int mNumberItems;
    final private ListItemClickListener mOnClickListener;
    private RoutesResponse mRoutesResponse;

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }


    RoutesAdapter(int numberItems, RoutesResponse routesResponse, ListItemClickListener mOnClickListener){
        mNumberItems = numberItems;
        mRoutesResponse = routesResponse;
        this.mOnClickListener = mOnClickListener;
    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.route_list_item;
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(layoutIdForListItem,parent, false);

        return new RouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RouteViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    /**
     * Class for view of Items
     */
    class RouteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView listItemRouteViewStart;
        TextView listItemRouteViewTime;
        TextView listItemRouteViewInfo;
        TextView listItemRouteViewEnd;
        View context;

        RouteViewHolder(View itemView) {
            super(itemView);
            context = itemView;
            listItemRouteViewStart = (TextView) itemView.findViewById(R.id.tv_item_route_start);
            listItemRouteViewTime = (TextView) itemView.findViewById(R.id.tv_item_route_time);
            listItemRouteViewInfo = (TextView) itemView.findViewById(R.id.tv_item_route_info);
            listItemRouteViewEnd = (TextView) itemView.findViewById(R.id.tv_item_route_end);

            itemView.setOnClickListener(this);
        }

        /**
         * Function to bind RecyclerViewItem to List item
         * @param listIndex Index of item
         */
        void bind(int listIndex) {
            RouteInfo routeInfo = null;
            StringBuilder buses = null;
            try {
                routeInfo = mRoutesResponse.getRoute(listIndex);
                List<VehicleRoute> vehicleRouteList = routeInfo.getBuses();

                buses = new StringBuilder(vehicleRouteList.get(0).getName());
                for (int i = 1; i < routeInfo.getNumBuses(); i++) {
                    buses.append(",").append(vehicleRouteList.get(i).getName());
                }
            } catch (NullPointerException e){
                Log.e("routeInfo", "error parsing time from routeInfo", e);
                return;
            }

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Time startTime = null;
            Time endTime = null;
            try {
                startTime = new Time(sdf.parse(routeInfo.getStartTime()).getTime());
                endTime = new Time(sdf.parse(routeInfo.getEndTime()).getTime());
            } catch (ParseException e) {
                Log.e("Time", "error parsing time from routeInfo", e);
                return;
            }

            listItemRouteViewStart.setText(mRoutesResponse.getRoute(listIndex).getStartTime());
            listItemRouteViewInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_directions_bus_black_24dp, 0, 0, 0);
            listItemRouteViewTime.setText((endTime.getTime()-startTime.getTime())/(1000*60) + context.getContext().getString(R.string.minutes) + "\n");
            listItemRouteViewInfo.setText(buses.toString());
            listItemRouteViewEnd.setText(mRoutesResponse.getRoute(listIndex).getEndTime());
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);
        }
    }
}
