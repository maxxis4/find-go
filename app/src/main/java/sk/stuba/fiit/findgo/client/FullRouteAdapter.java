package sk.stuba.fiit.findgo.client;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fiit.findgo.client.Data.BusStop;
import sk.stuba.fiit.findgo.client.Data.RouteInfo;
import sk.stuba.fiit.findgo.client.Data.VehicleRoute;

/**
 * Adapter for recycler view in FullRouteActivity
 * @author maxxis
 */
public class FullRouteAdapter extends RecyclerView.Adapter<FullRouteAdapter.FullRouteViewHolder> {
    private int mNumberItems;
    private RouteInfo mRouteInfo;

    FullRouteAdapter(int numberItems, RouteInfo routeInfo){
        mNumberItems = numberItems;
        mRouteInfo = routeInfo;
    }

    @Override
    public FullRouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.bus_list_item;
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(layoutIdForListItem,parent, false);

        return new FullRouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FullRouteViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    /**
     * ViewHolder for showing information in RecyclerView Item
     */
    class FullRouteViewHolder extends RecyclerView.ViewHolder {
        TextView listItemBusName;
        TextView listItemStartTime;
        TextView listItemEndTime;
        TextView listItemStopsCount;
        TextView listItemStartBusStopName;
        TextView listItemEndBusStopName;
        View context;

        FullRouteViewHolder(View itemView) {
            super(itemView);
            context = itemView;
            listItemBusName = (TextView) itemView.findViewById(R.id.tv_item_bus_name);
            listItemStartTime = (TextView) itemView.findViewById(R.id.tv_item_start_time);
            listItemEndTime = (TextView) itemView.findViewById(R.id.tv_item_end_time);
            listItemStopsCount = (TextView) itemView.findViewById(R.id.tv_item_stops_count);
            listItemStartBusStopName = (TextView) itemView.findViewById(R.id.tv_item_start_bus_stops_name);
            listItemEndBusStopName = (TextView) itemView.findViewById(R.id.tv_item_end_bus_stops_name);
        }

        /**
         * Function to bind item of recycler view to correct list item
         * @param listIndex Index of item in RecyclerView
         */
        void bind(int listIndex) {
            List<VehicleRoute> vehicleRoutes = mRouteInfo.getBuses();
            VehicleRoute vehicleRoute = vehicleRoutes.get(listIndex);
            int busStopsCount = vehicleRoute.getBusStops().size();
            String busStop = Integer.toString(busStopsCount) + " ";
            if(busStopsCount == 1){
                busStop += context.getResources().getString(R.string.bus_stop_sing);
            } else {
                busStop += context.getResources().getString(R.string.bus_stop_pl);
            }
            listItemStopsCount.setText(busStop);
            listItemBusName.setText(vehicleRoute.getName());
            listItemStartBusStopName.setText(vehicleRoute.getFirstBusStop().getName());
            listItemStartTime.setText(vehicleRoute.getFirstBusStop().getDeparture());
            listItemEndBusStopName.setText(vehicleRoute.getLastBusStop().getName());
            listItemEndTime.setText(vehicleRoute.getLastBusStop().getDeparture());
        }
    }
}
