package sk.stuba.fiit.findgo.client.Data;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * This class shows data about route from selected point A to B
 * @author maxxis
 */
public class RouteInfo implements Parcelable {
    public static final String INTENT_NAME = "RouteInfo";
    private LatLng mStart;
    private LatLng mDest;
    private String mDestName;
    private String mStartTime;
    private String mEndTime;
    private List<VehicleRoute> mBuses;

    public RouteInfo(String startTime, String endTime, List<VehicleRoute> buses) {
        mStartTime = startTime;
        mEndTime = endTime;
        mBuses = new ArrayList<>();
        mBuses.addAll(buses);
        mStart = mBuses.get(0).getFirstBusStop().getPosition();
        mDest = mBuses.get(mBuses.size() - 1).getLastBusStop().getPosition();
    }

    protected RouteInfo(Parcel in) {
        mStart = in.readParcelable(LatLng.class.getClassLoader());
        mDest = in.readParcelable(LatLng.class.getClassLoader());
        mDestName = in.readString();
        mStartTime = in.readString();
        mEndTime = in.readString();
        mBuses = new ArrayList<>();
        in.readTypedList(mBuses, VehicleRoute.CREATOR);
    }

    public static final Creator<RouteInfo> CREATOR = new Creator<RouteInfo>() {
        @Override
        public RouteInfo createFromParcel(Parcel in) {
            return new RouteInfo(in);
        }

        @Override
        public RouteInfo[] newArray(int size) {
            return new RouteInfo[size];
        }
    };

    public void setStart(LatLng start) {
        mStart = start;
    }

    public void setDest(LatLng start) {
        mDest = start;
    }

    public void setDestName(String name) {
        mDestName = name;
    }

    public LatLng getDest() {
        return mDest;
    }

    public String getDestName() {
        return mDestName;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public LatLng getStart() {
        return mStart;
    }

    public int getNumBuses() {
        if (mBuses != null) {
            return mBuses.size();
        }
        return 0;
    }

    public List<VehicleRoute> getBuses() {
        return mBuses;
    }

    public VehicleRoute getFirstBus() {
        return mBuses.get(0);
    }

    public VehicleRoute getLastBus() {
        return mBuses.get(getNumBuses() - 1);
    }

    public void addBus(VehicleRoute vehicleRoute) {
        mBuses.add(vehicleRoute);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mStart, flags);
        dest.writeParcelable(mDest, flags);
        dest.writeString(mDestName);
        dest.writeString(mStartTime);
        dest.writeString(mEndTime);
        dest.writeTypedList(mBuses);
    }
}
