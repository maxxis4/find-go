package sk.stuba.fiit.findgo.client;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import sk.stuba.fiit.findgo.client.Data.BusStop;
import sk.stuba.fiit.findgo.client.Data.RouteInfo;

/**
 * Activity to show full route with all buses to user
 * @author maxxis
 */
public class FullRouteActivity extends AppCompatActivity {

    private TextView mStartDistance;
    private TextView mEndDistance;

    private Button mStartNavigate;
    private Button mEndNavigate;

    private RecyclerView mBusesList;
    private FullRouteAdapter mFullRouteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_route);

        mStartDistance = (TextView) findViewById(R.id.tv_start_distance);

        mEndDistance = (TextView) findViewById(R.id.tv_end_distance);

        mStartNavigate = (Button) findViewById(R.id.bt_navigation_start);
        mEndNavigate = (Button) findViewById(R.id.bt_navigation_end);

        Intent intent = getIntent();
        final RouteInfo routeInfo = intent.getParcelableExtra(RouteInfo.INTENT_NAME);
        mStartNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusStop firstBusStop = routeInfo.getFirstBus().getFirstBusStop();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+routeInfo.getStart().latitude+","+routeInfo.getStart().longitude+"&daddr="+firstBusStop.getPosition().latitude+","+firstBusStop.getPosition().longitude+"&mode=walking"));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });

        String navigationStartMessage = getString(R.string.navigate_to) + routeInfo.getFirstBus().getFirstBusStop().getName();
        String navigationDestMessage = getString(R.string.navigate_to) + routeInfo.getDestName();
        mStartDistance.setText(navigationStartMessage);
        mEndDistance.setText(navigationDestMessage);

        int NUM_LIST_ITEMS = routeInfo.getNumBuses();
        mBusesList = (RecyclerView) findViewById(R.id.rv_routes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mBusesList.setLayoutManager(layoutManager);

        mEndNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusStop endBusStop = routeInfo.getLastBus().getLastBusStop();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+endBusStop.getPosition().latitude+","+endBusStop.getPosition().longitude+"&daddr="+routeInfo.getDest().latitude+","+routeInfo.getDest().longitude+"&mode=walking"));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });

        mFullRouteAdapter = new FullRouteAdapter(NUM_LIST_ITEMS, routeInfo);
        mBusesList.setAdapter(mFullRouteAdapter);


    }

    @Override
    /**
     * function to destroy Activity after going back to show all routes
     */
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
